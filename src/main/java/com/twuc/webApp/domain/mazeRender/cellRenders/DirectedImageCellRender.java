package com.twuc.webApp.domain.mazeRender.cellRenders;

import com.twuc.webApp.domain.mazeRender.Direction;
import com.twuc.webApp.domain.mazeRender.RenderCell;

import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * 该渲染器使用图片对迷宫节点进行渲染。在渲染的过程中会考虑节点之间的连通性。
 */
public class DirectedImageCellRender extends KeyedImageCellRender {

    /**
     * 创建一个 {@link DirectedImageCellRender} 实例。
     *
     * @param north 该节点和北方的节点是联通的。
     * @param south 该节点和南方的节点是联通的。
     * @param east 该节点和东方的节点是联通的。
     * @param west 该节点和西方的节点是联通的。
     * @param northSouth 该节点和北方与南方的节点是联通的。
     * @param northEast 该节点和北方与东方的节点是联通的。
     * @param northWest 该节点和北方与西方的节点是联通的。
     * @param southEast 该节点和南方与东方的节点是联通的。
     * @param southWest 该节点和南方与西方的节点是联通的。
     * @param eastWest 该节点和东方与西方节点是联通的。
     * @param northSouthEast 该节点和北方、南方、东方节点是联通的。
     * @param northSouthWest 该节点和北方、南方、西方节点是联通的。
     * @param northEastWest 该节点和北方、东方、西方节点是联通的。
     * @param southEastWest 该节点和南方、东方、西方节点是联通的。
     * @param northSouthEastWest 该节点和其他四个方向节点都联通。
     */
    public DirectedImageCellRender(
        BufferedImage north,
        BufferedImage south,
        BufferedImage east,
        BufferedImage west,
        BufferedImage northSouth,
        BufferedImage northEast,
        BufferedImage northWest,
        BufferedImage southEast,
        BufferedImage southWest,
        BufferedImage eastWest,
        BufferedImage northSouthEast,
        BufferedImage northSouthWest,
        BufferedImage northEastWest,
        BufferedImage southEastWest,
        BufferedImage northSouthEastWest) {
        super(createDirectedResources(
            north,
            south,
            east,
            west,
            northSouth,
            northEast,
            northWest,
            southEast,
            southWest,
            eastWest,
            northSouthEast,
            northSouthWest,
            northEastWest,
            southEastWest,
            northSouthEastWest));
    }

    private static HashMap<String, BufferedImage> createDirectedResources(
        BufferedImage north, BufferedImage south, BufferedImage east, BufferedImage west, BufferedImage northSouth,
        BufferedImage northEast, BufferedImage northWest, BufferedImage southEast, BufferedImage southWest,
        BufferedImage eastWest, BufferedImage northSouthEast, BufferedImage northSouthWest,
        BufferedImage northEastWest, BufferedImage southEastWest, BufferedImage northSouthEastWest) {
        HashMap<String, BufferedImage> map = new HashMap<>();

        map.put("North", north);
        map.put("South", south);
        map.put("East", east);
        map.put("West", west);
        map.put("NorthSouth", northSouth);
        map.put("NorthEast", northEast);
        map.put("NorthWest", northWest);
        map.put("SouthEast", southEast);
        map.put("SouthWest", southWest);
        map.put("EastWest", eastWest);
        map.put("NorthSouthEast", northSouthEast);
        map.put("NorthSouthWest", northSouthWest);
        map.put("NorthEastWest", northEastWest);
        map.put("SouthEastWest", southEastWest);
        map.put("NorthSouthEastWest", northSouthEastWest);
        map.put("Unknown", null);

        return map;
    }

    @Override
    protected boolean isSupported(RenderCell cell) {
        return cell.getDirection() != Direction.UNKNOWN;
    }

    @Override
    protected String getTextureKey(RenderCell cell) {
        int cellDirection = cell.getDirection();
        StringBuilder keyBuilder = new StringBuilder();
        if ((cellDirection & Direction.NORTH) == Direction.NORTH) keyBuilder.append("North");
        if ((cellDirection & Direction.SOUTH) == Direction.SOUTH) keyBuilder.append("South");
        if ((cellDirection & Direction.EAST) == Direction.EAST) keyBuilder.append("East");
        if ((cellDirection & Direction.WEST) == Direction.WEST) keyBuilder.append("West");

        return keyBuilder.length() == 0 ? "Unknown" : keyBuilder.toString();
    }
}
