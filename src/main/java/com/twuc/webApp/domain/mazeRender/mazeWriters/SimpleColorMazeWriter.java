package com.twuc.webApp.domain.mazeRender.mazeWriters;

import org.springframework.stereotype.Component;

@Component
public class SimpleColorMazeWriter extends NormalMazeWriter {
    public SimpleColorMazeWriter(ColorMazeComponentFactory factory) {
        super(factory);
    }

    @Override
    public String getName() {
        return "color";
    }
}

