package com.twuc.webApp.domain.mazeRender;

/**
 * 迷宫渲染网格的渲染类型。
 */
@SuppressWarnings("unused")
public enum RenderType {
    /**
     * 鬼知道这是什么类型。
     */
    UNKNOWN,

    /**
     * 墙。
     */
    GROUND,

    /**
     * 道路。
     */
    WALL
}
