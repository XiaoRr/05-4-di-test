package com.twuc.webApp.service;

import com.twuc.webApp.domain.mazeGenerator.AldousBroderMazeAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.DijkstraSolvingAlgorithm;
import com.twuc.webApp.domain.mazeGenerator.Grid;
import com.twuc.webApp.domain.mazeRender.MazeWriter;
import com.twuc.webApp.web.MazeController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class GameLevelServiceTest {

    @Mock
    MazeWriter mazeWriter;
    @Mock
    AldousBroderMazeAlgorithm aldousBroderMazeAlgorithm;
    @Mock
    DijkstraSolvingAlgorithm dijkstraSolvingAlgorithm;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void render_test() throws IOException {
        when(mazeWriter.getName()).thenReturn("color");

        doAnswer((Answer) invocation ->{
            OutputStream stream = invocation.getArgument(1);
            stream.write(new byte[]{1,2,3});
            return null;
        }).when(mazeWriter).render(any(Grid.class),any(OutputStream.class));

        List<MazeWriter> mazeWriters = new ArrayList<>();
        mazeWriters.add(mazeWriter);
        GameLevelService gameLevelService = new GameLevelService(mazeWriters,aldousBroderMazeAlgorithm,dijkstraSolvingAlgorithm);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        gameLevelService.renderMaze(outputStream,10,10,"color");
        byte[] bytes = outputStream.toByteArray();
        assertArrayEquals(bytes,new byte[]{1,2,3});

    }
}