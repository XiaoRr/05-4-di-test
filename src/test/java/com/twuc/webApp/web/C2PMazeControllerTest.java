package com.twuc.webApp.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc
@SpringBootTest
public class C2PMazeControllerTest {

    @Autowired
    MockMvc mockMvc;
    @Test
    void should_get_png_img_when_call_buffered_mazes() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/buffered-mazes/color")).andReturn();
        byte[] imgBytes = mvcResult.getResponse().getContentAsByteArray();
        assertArrayEquals(new byte[]{(byte) 137, 80, 78, 71, 13, 10, 26, 10}, Arrays.copyOfRange(imgBytes,0,8));

    }
}
