package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.stubbing.Answer;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

class MazeControllerTest {

    @Mock
    GameLevelService gameLevelService;
    @Mock
    MockHttpServletResponse response;
    MazeController mazeController;
    @BeforeEach
    void setUp() {
       initMocks(this);
       mazeController = new MazeController(gameLevelService);
    }

    @Test
    void should_call_renderMaze() throws IOException {
        mazeController.getMaze(10,10,"glass");
        verify(gameLevelService,times(1)).renderMaze(10,10,"glass");
    }

    @Test
    void should_call_renderMazeCorrectly() throws IOException {
        doAnswer((Answer) invocation ->{
            int width = invocation.getArgument(0);
            int height = invocation.getArgument(1);
            String type = invocation.getArgument(2);

            assertEquals(10,width);
            assertEquals(10,height);
            assertEquals("glass",type);
            return null;
        }).when(gameLevelService).renderMaze(any(OutputStream.class),anyInt(),anyInt(),anyString());
        mazeController.getMaze2(response,10,10,"glass");
    }
}