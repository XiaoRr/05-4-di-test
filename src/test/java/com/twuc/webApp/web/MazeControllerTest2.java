package com.twuc.webApp.web;

import com.twuc.webApp.service.GameLevelService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@AutoConfigureMockMvc
@SpringBootTest
class MazeControllerTest2 {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    GameLevelService gameLevelService;

    MazeController mazeController;

    @BeforeEach
    void setUp() {
        initMocks(this);
        mazeController = new MazeController(gameLevelService);
    }

    @Test
    void getMaze() throws Exception {
        when(gameLevelService.renderMaze(anyInt(),anyInt(),anyString())).thenReturn(new byte[]{1,2,3});
        mockMvc.perform(get("/buffered-mazes/color")).andExpect(content().bytes(new byte[]{1,2,3}));
        verify(gameLevelService,times(1)).renderMaze(anyInt(),anyInt(),anyString());
    }

    @Test
    void getMazePng() throws Exception {
        when(gameLevelService.renderMaze(anyInt(),anyInt(),anyString())).thenReturn(new byte[]{1,2,3});
        mockMvc.perform(get("/buffered-mazes/color").param("width","8")
        .param("height","8")).andExpect(content().bytes(new byte[]{1,2,3}));
        verify(gameLevelService,times(1)).renderMaze(8,8,"color");
    }
}